( () => {
  
  const _str_host = window.location.host; // ~ 127.0.0.1:8080

  // default not avail image
  let IMAGE_NOT_AVAIL = "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available";
  
  let $results = document.querySelector("#results");
  let $status = document.querySelector("#status");

  let start = 1988; // let start = 2013;
  
  let end = 1986; // let end = 2011;
  
  let deferreds = [];
  
  let dataFulfilled = null;
  let asyncHBSTemplate = '';
  let templateName = "comic";
  
  const urlOps = () => { 
    console.log( "_str_host" , _str_host ); // 127.0.0.1:8080 
  };

  const thrown = (derrp) => {
    // console.log( "derrp.message" , derrp.message );
    return Promise.reject( derrp.message );
  };
  
  const getRandomInt = ( min, max ) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor( Math.random() * (max-min+1) ) + min;
  };

  const getComicData = ( year ) => {
  
    let baseUrl = 'https://gateway.marvel.com:443/v1/public';
    let path = '/comics';
    let TS = new Date().getTime();
    let KEY = 'ABC';
    let stringToHash = `${TS}DEF${KEY}`;
    let ALPHAHASH = CryptoJS.MD5(stringToHash);
    let requiredparams = `?apikey=${KEY}&ts=${TS}&hash=${ALPHAHASH}`;    
    
    let optionalparams = `&limit=100&format=comic&formatType=comic&dateRange=${year}-01-01%2C${year}-12-31`; 
  
    let url = `${baseUrl}${path}${requiredparams}${optionalparams}`; 
    
    const response = fetch(url)
    .then( response => {
      // return response.json();
      return Promise.resolve( response.json() );
    } )
    .catch( err => {
      $status.innerHTML = ""; 
      // console.log( "err.message", err.message );
      let derrp = new Error( err.message );
      $status.insertAdjacentHTML("beforeend" , derrp ); // $status.innerHTML = derrp; 
      return Promise.reject( derrp ); // throw derrp;
    } );
    
    return response;
  };

  const promiseOne = () => {
    return new Promise( (fnresolve, fnreject) => {
    
      const goodfunk = () => {
        $results.innerHTML = "";
        $status.innerHTML = ""; 
        let initialhtml = "<i>Retrieving your data. Hold please.</i>";
        $status.insertAdjacentHTML("beforeend" , initialhtml ); // $status.innerHTML = initialhtml; 
        for(let x=start; x>=end; x--){
          deferreds.push( getComicData(x) );
          // console.log( 'getComicData(' + x + ')' );
        }
  
        // console.log( "deferreds: " , deferreds );
        return deferreds;
      };
  
      fnresolve( goodfunk() ); 
      // fnreject( throw new Error("You dun effed up!!!") ); 
  
    });
  };

  const templateGrabber = (templateName) => {
    
    // const url = `../templates/${templateName}.handlebars`; 
    // let url;

    // if( _str_host === 'artavia.gitlab.io' ){
    //   url = `templates/${templateName}.handlebars`; 
    // }
    // else{
    //   url = `../templates/${templateName}.handlebars`; 
    // }

    const url = `templates/${templateName}.handlebars`; 

    let myRequest = new Request( url );
    let myHeaders = new Headers();
    myHeaders.append( 'Content-type' , 'text/x-handlebars-template; charset=UTF-8' ); 
    myHeaders.append( "Accept" , "text/x-handlebars-template" );

    let optionsobject = { headers: myHeaders };

    const response = fetch( myRequest, optionsobject )
    .then( response => {
      // return response.text();
      return Promise.resolve( response.text() );
    } )
    .then( (ajax) => {

      asyncHBSTemplate = ajax; 
      asyncHBSTemplate = Handlebars.compile( asyncHBSTemplate );
      // console.log( "asyncHBSTemplate" , asyncHBSTemplate );

      return asyncHBSTemplate;
    } )
    .catch( err => {
      $status.innerHTML = ""; 
      // console.log( "err.message", err.message );
      let derrp = new Error( err.message );
      $status.insertAdjacentHTML("beforeend" , derrp ); // $status.innerHTML = derrp; 
      return Promise.reject( derrp ); // throw derrp;
    } );

    return response;    
  };
  
  const extractPromiseObjects = ( pmFulfill ) => {
    if( pmFulfill === null ){
      return pmFulfill = templateGrabber( templateName );
    }
    // console.log( "pmFulfill" , pmFulfill );
  };

  const setupTemplates = ( fulfilledBool ) => {

    Handlebars.registerHelper( "SAFER_thumbs" , ( thumbobjects ) => {
      
      link = Handlebars.Utils.escapeExpression(thumbobjects.link);
      title = Handlebars.Utils.escapeExpression(thumbobjects.title);
      thumb = Handlebars.Utils.escapeExpression(thumbobjects.thumb);

      const result = `
        <a href="${link}" class="comicanchor" title="Link to details about ${title}" target="_blank" rel="noopener noreferrer">
        <img src="${thumb}" class="thumb" alt="Thumbnail picture of ${title}" />
        </a>
      `;

      return new Handlebars.SafeString( result );

    } );
    
    return extractPromiseObjects( fulfilledBool );
  };

  const promiseTwo = () => {
    return new Promise( (fnresolve, fnreject) => {

      let config = {};
      config.dataFulfilled = dataFulfilled; 
    
      const goodfunk = (pm) => {
        return setupTemplates( pm.dataFulfilled );
      };
  
      fnresolve( goodfunk( config ) ); 
      // fnreject( throw new Error("You dun effed up!!!") ); 
  
    });
  };

  const alpha = () => {
    return Promise.all( [ promiseOne(), promiseTwo() ] ); 
  };

  const bagofcats = async ( response ) => {

    // console.log( "bagofcats response", response ); // fulfilled OR rejected

    let comicUrls = []; 

    let arrayOfYears = response[0]; 
    
    for( let el of arrayOfYears ){
      
      // console.log( "el" , await el ); // "attributes/properties" are STILL inaccessible
  
      comicUrls.push( await el );
    }

    // console.log( "asyncHBSTemplate" , asyncHBSTemplate );
  
    return comicUrls; 

  };

  const charlie = (arrayofdata) => {
      
    // console.log( "arrayofdata" , arrayofdata ); // "attributes/properties" are NOW accessible

    let arrayOfYears = [];
    let arrayOfStats = [];
    let arrayOfResponseObjects = []
  
    $status.innerHTML = ""; 
    let statushtml = "";
    $status.insertAdjacentHTML("beforeend" , statushtml ); // $status.innerHTML = statushtml; 
  
    for(let x=0; x<arrayofdata.length; x++){
      
      let year = start-x;
      // console.log("displaying year", year); 

      arrayOfYears.push( year );
      
      let stats = {};
      stats.year = year;
      stats.priceTotal = 0;
      stats.priceCount = 0;
      stats.minPrice = 999999999;
      stats.maxPrice = -999999999;
      stats.pageTotal = 0;
      stats.pageCount = 0;
      stats.pics = [];

      stats.allTitles = [];
      stats.allDetailUrls = [];
      
      arrayOfStats.push( stats );
  
      let res = arrayofdata[x];
      // console.log( "----- res", res );

      arrayOfResponseObjects.push( res );
  
    }
    
    // return arrayofdata;
    return {
      arrayOfResponseObjects
      , arrayOfStats
    };
    
  };
  
  const delta = ( { arrayOfResponseObjects , arrayOfStats } ) => {
    
    let arrayOfFilteredItems = []

    // console.log( "arrayOfResponseObjects" , arrayOfResponseObjects );
    // console.log( "arrayOfStats" , arrayOfStats);

    arrayOfResponseObjects.map( (el, idx, arr ) => {
      if( el.code === 200 ){
        
        let myResults = el.data.results;
        
        let filtered = myResults.filter( (comic,idx,arr) => {
          return (comic.thumbnail.path !== IMAGE_NOT_AVAIL) && ( comic.title !== undefined );
        } );

        // console.log( "filtered", filtered );
        // console.log( "filtered.length", filtered.length );

        arrayOfFilteredItems.push( filtered );
      }
    } ); 

    return {
      arrayOfStats , arrayOfFilteredItems
    }

  };

  const elephant = ( { arrayOfStats , arrayOfFilteredItems } ) => {
    
    // console.log( "arrayOfStats" , arrayOfStats );
    // console.log( "arrayOfFilteredItems" , arrayOfFilteredItems );
    
    arrayOfStats.map( (stat,idx,arr) => {
      
      // console.log( "stat", stat ); 
      
      let mags = arrayOfFilteredItems[idx];
      // console.log( "mags" , mags );

      return mags.map( (comic,idx,arr) => {
        // console.log( "comic" , comic );
        
        
        if(comic.prices.length && comic.prices[0].price !== 0) {
  
          stat.priceTotal += comic.prices[0].price;

          if(comic.prices[0].price > stat.maxPrice) {
            stat.maxPrice = comic.prices[0].price;
          }

          if(comic.prices[0].price < stat.minPrice) {
            stat.minPrice = comic.prices[0].price;
          }

          stat.priceCount++;
        }

        if(comic.pageCount > 0) {
          stat.pageTotal+=comic.pageCount;
          stat.pageCount++;
        }
        
        // if(comic.thumbnail && comic.thumbnail.path !== IMAGE_NOT_AVAIL) {
          stat.pics.push(`${comic.thumbnail.path}.${comic.thumbnail.extension}`);
        // }

        stat.allTitles.push( comic.title );
        stat.allDetailUrls.push( comic.urls[0].url );

        // console.dir( "stat" , stat );
        
        return stat;

      } );

    } );

    // console.log( "arrayOfStats" , arrayOfStats );

    return arrayOfStats;

  };

  const foxtrot = ( arr ) => {
    
    // console.log( "arr", arr ); 

    let stats = arr.map( (stat, idx, arr ) => {
      
      stat.avgPrice = (stat.priceTotal/stat.priceCount).toFixed(2);
      stat.avgPageCount = Math.floor( (stat.pageTotal/stat.pageCount).toFixed(2) );

      stat.thumbs = [];
      stat.titles = [];
      stat.detailUrls = []; 
      stat.thumbobjects = [];
      
      
      // while( stat.pics.length > 0 ) { // pick all thumbnails 
      // while( stat.pics.length > 0 && stat.thumbs.length < 10 ) { // pick 10 thumbnails at random
      // while( stat.pics.length > 0 && stat.thumbs.length < 7 ) { // pick 7 thumbnails at random
      
      while( stat.pics.length > 0 && stat.thumbs.length < 5) { // pick 5 thumbnails at random
        
        let chosen = getRandomInt( 0, stat.pics.length );

        stat.thumbs.push( stat.pics[chosen] );
        stat.pics.splice( chosen, 1 );

        stat.titles.push( stat.allTitles[chosen] );
        stat.allTitles.splice( chosen, 1 );

        stat.detailUrls.push( stat.allDetailUrls[chosen] );
        stat.allDetailUrls.splice( chosen, 1 );

      }

      stat.thumbs.map( (el,idx,arr) => {
        
        let thumbinstance = el;
        let titleinstance = stat.titles[idx];
        let detailurlinstance = stat.detailUrls[idx];
        
        return stat.thumbobjects.push( {
          thumb: thumbinstance
          , title: titleinstance
          , link: detailurlinstance
        } );

      } );
      
      // console.log( "stat", stat );
      // console.log( "stat.thumbobjects", stat.thumbobjects );
      
      return stat;

    } );

    return stats;

  };

  const golf = ( parsedResults ) => {
    
    // console.log( "parsedResults", parsedResults ); 

    let html = asyncHBSTemplate( { comics: parsedResults } );
    
    $results.insertAdjacentHTML("beforeend" , html ); 
    // $results.innerHTML = html; 

  };

  const initFunction = () => { 
    
    alpha()
    .then( bagofcats )
    .then( charlie )
    .then( delta )
    .then( elephant )
    .then( foxtrot )
    .then( golf )
    .catch( thrown ); 

    // urlOps();
  
  };
  
  document.addEventListener( "DOMContentLoaded", initFunction, false ); 

} )();
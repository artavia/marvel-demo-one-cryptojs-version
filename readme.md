# Marvel Demo One

## Description
Marvel demo one originally by Ray Camden&hellip; re&#45;expressed by yours truly.

## Re&#45;expressed? 
You probably just asked yourself, &quot;dafuq izzat&quot;. Well&hellip; it contains a little more styling than the original lesson. Plus, the javascript is a little more deliberate. Finally, the handlebars template is separated from the html.

## For use with Netlify
This presentation is of the &quot;manual deployment&quot; in conjunction with drag&#45;drop for changes to take effect. 

To make a long story short, **I can just as easily use the CLI or drag and drop this prezzo**. 

In essence, today I decided that I [would] navigate to said project folder in my local environment, and simultaneously locate the "Production deploys" section in the [Netlify dashboard](https://app.netlify.com "link to netlify dashboard"), and consequently drag-and-drop said project into the bucket in order for the changes to take effect. **Are you feeling me, yet?!**

## Please visit
You can see [the lesson at netlify](https://marvel-cryptojs-demo.netlify.com/ "the lesson at netlify") and decide if this is something for you to play around with at a later date.

## Relevant experience
For me this article is what got the ball rolling in terms of JAM stack as explained by [Ray Camden](https://www.raymondcamden.com/2014/02/02/Examples-of-the-Marvel-API "link to Ray Camden&rsquo;s blog"). 

  - For a non nodejs project, [Crypto&#45;JS](https://code.google.com/archive/p/crypto-js/ "link to archive") is your best friend! ~~I got an opportunity to play with the [crypto module](https://nodejs.org/api/crypto.html "link to crypto documentation").~~
    - To the best of my knowledge, there at least two individual lessons that are still relevant ([link to lesson 1](https://www.c-sharpcorner.com/blogs/advanced-encryption-in-javascript-using-cryptojs "link to CryptoJs lesson one")) ([link to lesson 2](https://www.davidebarranca.com/2012/10/crypto-js-tutorial-cryptography-for-dummies/ "link to CryptoJs lesson two")).
  - The Marvel Comics api is just a toy but it has a couple of valuable lessons contained underneath the hood. **Communication** to the api is **impossible** without some form of **cryptographical obfuscation** during the initial call to said api.
  - Last, it gives me another reason to use netlify.

## My thoughts on Marvel comics, et. al.
I am not endorsing Marvel or any of the witchcraft and nefarious works of death that go along with it. 

I used to collect comics when I was a teenager growing up during the 80s (when I was lost and knew no better). If you are into Marvel or DC or any other of that **&quot;harmless artistic fantasy&quot;** in the year 2020, then, you should be worried because the truth is that it is evil, it is dirty and it is antichrist. 

**&quot;Marvel Not&quot;** and do the following if you are not convinced yet:
  - **Know your enemy** and view [this presentation in English on Youtube](https://www.youtube.com/watch?v=qbCbyf16iYc "Marvel not in English") (authored done by Little Light Studios) because it was eye&#45;opening to say the least and it reinforces a lot of what I initially suspected about these so-called &quot;brands&quot;;
  - or, you can visit this subsequent [presentation in Spanish on Youtube](https://www.youtube.com/watch?v=leRAsF7NmNo "Marvel not in Spanish") (authored by Dilo Al Mundo) in order to get an eyefull. Nobody is trying to fool you: **you only have to look at the comments** to gain a perspective;

And, if you think true Christianity is a joke and something to be scoffed at then you can go on and pursue your commerce, way of life, unrighteousness, wickedness and disobedience&#45;&#45; you are still **dead wrong** in the end!!! If you like worshipping false idols, then, you can enjoy a cup full of God&rsquo;s wrath, as well, cuz **you are just a bunch of suckers**! 

To the rest of you [feckless baal worshipers who like to worship ~~other idols~~ la negrita](https://www.youtube.com/watch?v=7PhlOsxfM_Y "the baal worshipers of today"), I would like to rhetorically ask you all a question because you have no shame and I want you to indulge me for a moment: **&hellip;are you ready for what comes next?**

I challenge any of you non-christians and biblical &quot;dogs&quot; to call on your precious so-called **Avengers**, or invoke your **psychology** while you are at it during **your** most dangerous hour. Go on! Say you&rsquo;re scared! I dare you! 

As for me and my household, we will continue to call on the precious blood of Jesus for protection!

Therefore, to [all] the fools who like the parrot the line in March 2020 that **we are all in this Coronavirus crisis together**, I call your bluff and rebuke you in the name of the Lord Jesus Christ. 

Meanwhile, you can enjoy **your** plagues and **your** pestilences because I will tell you that the **Lord Jesus Christ** who is the **incorruptible God** is my family's protection. 

I will stay protected in the cleft of the rock in He who is the Son of God; in He that is my safety and protection while my feet are planted on solid ground by trusting the Lord Jesus Christ as his indignation passes over my home. 

